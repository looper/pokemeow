﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Pokemeow.Engine.Plugin.Api
{
    /// <summary>
    ///     Can be used to load and retrieve all mods.
    /// </summary>
    public sealed class ModManager
    {
        private const string PluginPath = "./mods";

        /// <summary>
        ///     All mods that are loaded by this mod-manager. It's not guaranteed that the mods are initialized.
        /// </summary>
        public IEnumerable<IMod> LoadedMods { get; private set; } = new IMod[0];

        /// <summary>
        ///     Loads all Mods that can be found.
        /// </summary>
        public void LoadMods()
        {
            if (!Directory.Exists(PluginPath))
                return;
            var mods = new List<IMod>();

            foreach (var pluginFile in Directory.GetFiles(PluginPath, "*.dll"))
            {
                var assembly = Assembly.LoadFrom(pluginFile);
                foreach (var type in assembly.GetTypes())
                {
                    if (type.IsAbstract || type.IsInterface || type.IsSubclassOf(typeof (IMod)))
                        continue;
                    mods.Add((IMod) Activator.CreateInstance(type));
                }
            }
            LoadedMods = mods.AsReadOnly();
        }
    }
}