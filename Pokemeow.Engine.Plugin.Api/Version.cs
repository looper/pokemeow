﻿using System;

namespace Pokemeow.Engine.Plugin.Api
{
    /// <summary>
    ///     Can be used to display a version, following the semver-specification.
    /// When comparing two versions with each other, the 
    /// </summary>
    public sealed class Version : IComparable<Version>
    {
        public Version(uint major, uint minor = 0, uint patch = 0, string extension = "")
        {
            Major = major;
            Minor = minor;
            Patch = patch;
            Extension = extension;
        }

        public uint Major { get; }
        public uint Minor { get; }
        public uint Patch { get; }
        public string Extension { get; }

        public int CompareTo(Version other)
        {
            var major = Major.CompareTo(other.Major);
            var minor = Minor.CompareTo(other.Minor);
            var patch = Patch.CompareTo(other.Patch);
            if (major != 0)
                return major;
            if (minor != 0)
                return minor;
            if (patch != 0)
                return patch;
            return 0;
        }

        public override string ToString()
        {
            return $"{Major}.{Minor}.{Patch}-{Extension}";
        }

        public override bool Equals(object obj)
        {
            var other = obj as Version;
            return other != null && Major == other.Major && Minor == other.Minor && Patch == other.Patch &&
                   string.Equals(Extension, other.Extension);
        }

        public override int GetHashCode()
        {
            var hashCode = (int) Major;
            hashCode = (hashCode*397) ^ (int) Minor;
            hashCode = (hashCode*397) ^ (int) Patch;
            hashCode = (hashCode*397) ^ (Extension?.GetHashCode() ?? 0);
            return hashCode;
        }
    }
}