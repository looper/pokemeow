﻿using System;

namespace Pokemeow.Engine.Plugin.Api
{
    /// <summary>
    ///     Interface that all mods have to implement. It's used to give information about the mod and as a starting-point for
    ///     everything the mod implements.
    /// </summary>
    public interface IMod
    {
        /// <summary>
        ///     An unique identifier for the mod. The same identifier must not be used for two mods.
        /// </summary>
        string Identifier { get; }

        /// <summary>
        ///     The name of the mod. Will be used to display it for the user.
        /// </summary>
        string Name { get; }

        /// <summary>
        ///     The description of the mod for the user.
        /// </summary>
        string Description { get; }

        /// <summary>
        ///     The version of the mod.
        /// </summary>
        Version Version { get; }

        /// <summary>
        ///     Initializes the mod.
        /// </summary>
        /// <param name="services">The global service provider. This provider is shared by all mods and core-classes.</param>
        void Initialize(IServiceProvider services);
    }
}