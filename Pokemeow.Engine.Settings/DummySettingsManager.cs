﻿// TODO replace

using System.Collections.Generic;

namespace Pokemeow.Engine.Settings
{
    /// <summary>
    ///     A dummy manager for testing purposes.
    /// </summary>
    public class DummySettingsManager : ISettingsManager
    {
        private readonly Dictionary<string, object> _settings;

        public DummySettingsManager()
        {
            _settings = new Dictionary<string, object>
            {
                {"client.graphics.screen.width", 800},
                {"client.graphics.screen.height", 640}
            };
        }

        public T Load<T>(string key)
        {
            return (T) _settings[key];
        }

        void ISettingsManager.Write<T>(string key, T value)
        {
        }

        public bool HasSetting<T>(string key)
        {
            return _settings.ContainsKey(key);
        }

        public void Flush()
        {
        }
    }
}