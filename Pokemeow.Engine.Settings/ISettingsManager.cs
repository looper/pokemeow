﻿namespace Pokemeow.Engine.Settings
{
    /// <summary>
    ///     Provides access to stored settings.
    ///     Implementations may choose to defer the storage of properties until Flush is called.
    /// </summary>
    public interface ISettingsManager
    {
        /// <summary>
        ///     Loads the setting associated with the specified key.
        /// </summary>
        /// <typeparam name="T">The type of the setting</typeparam>
        /// <param name="key">The key that identifies the setting</param>
        /// <returns>The loaded setting or default(T) when no such setting could be found</returns>
        T Load<T>(string key);

        /// <summary>
        ///     Stores the given setting.
        /// </summary>
        /// <typeparam name="T">The type of the setting</typeparam>
        /// <param name="key">The key which should identify the setting</param>
        /// <param name="value">The setting that should be stored</param>
        void Write<T>(string key, T value);

        /// <summary>
        ///     Checks whether a setting with the given key and type is given.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        bool HasSetting<T>(string key);

        /// <summary>
        ///     Flushes all changes to the backing storage.
        /// </summary>
        void Flush();
    }
}