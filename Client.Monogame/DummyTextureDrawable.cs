﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using IDrawable = Pokemeow.Engine.Graphics.IDrawable;

namespace Client.Monogame
{
    /// <summary>
    /// TODO replace with correct implementation
    /// </summary>
    public class DummyTextureDrawable : IDrawable, IDisposable
    {
        private readonly Texture2D _texture;

        public DummyTextureDrawable(Texture2D texture)
        {
            _texture = texture;
        }

        public void Dispose()
        {
            _texture.Dispose();
        }

        public void Draw(TimeSpan elapsedTime)
        {
            throw new NotImplementedException();
        }

        public void Draw(TimeSpan elapsedTime, Rectangle targetRectangle, SpriteBatch batch)
        {
            // Tiling the texture is not enabled by default, so we need to reimplement it

            var tileWidth = _texture.Width;
            var tileHeight = _texture.Height;

            for (var xOffset = 0; xOffset*tileWidth < targetRectangle.Width; xOffset++)
            {
                for (var yOffset = 0; yOffset*tileHeight < targetRectangle.Height; yOffset++)
                {
                    var target = new Rectangle(targetRectangle.Left + xOffset*tileWidth, targetRectangle.Top + yOffset * tileHeight, tileWidth, tileHeight);//.Intersection(targetRectangle);
                    
                    batch.Draw(_texture, target, Color.White);
                }
            }
        }
    }

    public static class RectangleUtil
    {
        public static Rectangle Intersection(this Rectangle rect1, Rectangle rect2)
        {
            var top = rect1.Top < rect2.Top ? rect2.Top : rect1.Top;
            var bottom = rect1.Bottom > rect2.Bottom ? rect2.Bottom : rect1.Bottom;
            var left = rect1.Left < rect2.Left ? rect2.Left : rect1.Left;
            var right = rect1.Right > rect2.Right ? rect2.Right : rect1.Right;
            return new Rectangle(top, bottom, left, right);
        }
    }
}