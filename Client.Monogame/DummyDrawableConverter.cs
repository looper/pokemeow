﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Pokemeow.Engine;
using Pokemeow.Engine.Graphics;
using Pokemeow.Engine.World;

namespace Client.Monogame
{
    public class DummyDrawableConverter : IDrawableConverter
    {
        private readonly IServiceProvider _services;

        public DummyDrawableConverter(IServiceProvider services)
        {
            _services = services;
        }

        public IDrawable Convert(IDrawable drawable, string id)
        {
            var content = (ContentManager) _services.GetService(typeof (ContentManager));
            var convertedDrawable = new DummyTextureDrawable(content.Load<Texture2D>(id));
            ((DisposableContainer) _services.GetService(typeof (DisposableContainer))).Add(convertedDrawable);
            return convertedDrawable;
        }
    }
}