﻿using System;
using Client.Common.Scenes;
using Client.Monogame.Camera;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pokemeow.Engine.Input;
using Pokemeow.Engine.World;

namespace Client.Monogame.Scenes
{
    /// <summary>
    ///     A scene that shows the player in the world. Is responsible to update and render everything that happens on a map.
    /// </summary>
    public class WorldScene : IScene
    {
        private readonly IServiceProvider _services;

        public WorldScene(IServiceProvider services)
        {
            _services = services;
        }

        public bool IsInitialized { get; private set; }

        public string Name => "WorldScene";

        public void Initialize()
        {
            // TODO
            IsInitialized = true;
        }

        public void DeInitialize()
        {
            // TODO
            IsInitialized = false;
        }

        public void Enter()
        {
        }

        public void Leave()
        {
        }

        public void Draw(TimeSpan elapsedTime)
        {
            var camera = (Camera2D) _services.GetService(typeof (Camera2D));
            var batch = (SpriteBatch) _services.GetService(typeof (SpriteBatch));
            var viewport =
                ((GraphicsDeviceManager) _services.GetService(typeof (GraphicsDeviceManager))).GraphicsDevice.Viewport;

            batch.Begin(transformMatrix: camera.GetTransformationMatrix(viewport));

            var worldService = (IWorldService) _services.GetService(typeof (IWorldService));
            worldService.GetActiveMap().Draw(elapsedTime);
            batch.End();
        }

        public void Update(GameTime elapsedTime)
        {
            var controller = (IController) _services.GetService(typeof (IController));
            controller.Update(elapsedTime);
            var worldService = (IWorldService) _services.GetService(typeof (IWorldService));
            worldService.GetActiveMap().Update(elapsedTime);
            var camera = (Camera2D) _services.GetService(typeof (Camera2D));
            camera.Update(elapsedTime);
        }
    }
}