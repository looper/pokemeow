﻿using System;
using System.Linq;
using Client.Common.Scenes;
using Client.Monogame.Pipeline;
using Client.Monogame.Plugin;
using Client.Monogame.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pokemeow.Engine;
using Pokemeow.Engine.Plugin.Api;
using Pokemeow.Engine.Settings;

namespace Client.Monogame
{
    /// <summary>
    ///     Main type for Pokemeow.
    /// </summary>
    public class Pokemeow : Game
    {
        /// <summary>
        ///     Initializes the client.
        /// </summary>
        public Pokemeow()
        {
            Content = new CustomContentManager(Services, "Content");
        }

        /// <summary>
        ///     Allows the game to perform any initialization it needs to before starting to run.
        ///     This is where it can query for any required services and load any non-graphic
        ///     related content.  Calling base.Initialize will enumerate through any components
        ///     and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            CheckRequiredServices();
            LoadGraphicsSettings();

            Services.GetService<ISceneManager>().EnterScene<WorldScene>();
            Services.GetService<ModManager>().LoadMods();
            var loadedMods = Services.GetService<ModManager>().LoadedMods;
            foreach (var mod in loadedMods)
            {
                mod.Initialize(Services);
            }

            //TODO replace this with AddAll ExtensionMethod for Components (Collection)
            foreach (var component in Services.GetService<IGameComponentList>().GetAllComponents())
            {
                Components.Add(component);
            }

            base.Initialize();
        }

        /// <summary>
        ///     Loads the graphics-settings and applies them.
        /// </summary>
        private void LoadGraphicsSettings()
        {
            var settings = Services.GetService<ISettingsManager>();
            var graphics = Services.GetService<GraphicsDeviceManager>();
            if (settings.HasSetting<int>("client.graphics.screen.width"))
                graphics.PreferredBackBufferWidth = settings.Load<int>("client.graphics.screen.width");
            if (settings.HasSetting<int>("client.graphics.screen.width"))
                graphics.PreferredBackBufferHeight = settings.Load<int>("client.graphics.screen.height");
            graphics.ApplyChanges();
        }

        /// <summary>
        ///     Checks whether all required services can be found with the service manager.
        /// </summary>
        private void CheckRequiredServices()
        {
            // TODO Decide whether to remove this.
            var neededServices = new[]
            {
                typeof (ISettingsManager), typeof (GraphicsDeviceManager), typeof (ISceneManager), typeof (ModManager),
                typeof (IGameComponentList)
            };
            if (neededServices.Any(s => Services.GetService(s) == null))
            {
                throw new Exception("Not all services are loaded.");
            }
        }

        /// <summary>
        ///     LoadContent will be called once per game and is the place to load
        ///     all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            var spriteBatch = new SpriteBatch(GraphicsDevice);
            Services.AddService(spriteBatch);
        }

        /// <summary>
        ///     UnloadContent will be called once per game and is the place to unload
        ///     game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            ((DisposableContainer) Services.GetService(typeof (DisposableContainer))).Dispose();
        }

        /// <summary>
        ///     Allows the game to run logic such as updating the world,
        ///     checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            Services.GetService<ISceneManager>().Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        ///     This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            Services.GetService<ISceneManager>().Draw(gameTime.ElapsedGameTime);
            base.Draw(gameTime);
        }
    }
}