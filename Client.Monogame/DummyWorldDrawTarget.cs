﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pokemeow.Engine.World;
using IDrawable = Pokemeow.Engine.Graphics.IDrawable;

namespace Client.Monogame
{
    public class DummyWorldDrawTarget : IWorldDrawTarget
    {
        private readonly IServiceProvider _services;

        public DummyWorldDrawTarget(IServiceProvider services)
        {
            _services = services;
        }

        public void Draw(TimeSpan elapsedTime, IDrawable drawable, Rectangle bounds)
        {
            var spriteBatch = (SpriteBatch) _services.GetService(typeof (SpriteBatch));
            var dummyDrawable = drawable as DummyTextureDrawable;
            var target = new Rectangle(bounds.X*16, bounds.Y*16, bounds.Width*16, bounds.Height*16);
            dummyDrawable?.Draw(elapsedTime, target, spriteBatch);
        }
    }
}