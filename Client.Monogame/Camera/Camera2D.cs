﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pokemeow.Engine.World;

namespace Client.Monogame.Camera
{
    /// <summary>
    ///     The camera can be used to calculate an offset for the spritebatch, so "what the user can see" can be moved without
    ///     messing with the world's coordinates.
    /// </summary>
    public class Camera2D : IGameComponent, ICamera
    {
        /// <summary>
        ///     The center of the camera.
        /// </summary>
        private Vector2 _position = Vector2.One;

        /// <summary>
        ///     The zoom-factor of the camera. Zoom = 1 means that the graphics are in original size.
        /// </summary>
        private float _zoom = 1F;

        private ICameraTarget _target;

        /// <summary>
        ///     How much the camera should zoom. A value of 1 means 100% of the original size.
        /// </summary>
        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value <= 0 ? 0.1F : value; }
        }

        /// <summary>
        ///     The rotation of the camera in radians.
        /// </summary>
        public float Rotation { get; set; } = 0F;

        public void Initialize()
        {
        }

        /// <summary>
        ///     Changes the position of the camera so that the camera looks at the given coordinate. The position is in the center
        ///     of the camera.
        /// </summary>
        /// <param name="position"></param>
        public void LookAt(Vector2 position)
        {
            _position = position;
        }

        /// <summary>
        ///     Calculates the matrix that describes the transformation to the spriteBatch.
        /// </summary>
        /// <param name="viewport">The viewport of the screen</param>
        /// <returns></returns>
        public Matrix GetTransformationMatrix(Viewport viewport)
        {
            return Matrix.CreateTranslation(new Vector3(-_position.X, -_position.Y, 0))
                   *Matrix.CreateRotationZ(Rotation)
                   *Matrix.CreateScale(Zoom)
                   *Matrix.CreateTranslation(viewport.Width*0.5F, viewport.Height*0.5F, 0);
        }

        public void Update(GameTime elapsedTime)
        {
            if (_target == null)
                return;
            LookAt(_target.Position);
        }

        public void Follow(ICameraTarget target)
        {
            _target = target;
        }
    }
}