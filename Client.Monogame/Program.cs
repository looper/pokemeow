﻿using System;
using Client.Common.Scenes;
using Client.Monogame.Camera;
using Client.Monogame.Controller;
using Client.Monogame.Pipeline;
using Client.Monogame.Pipeline.Loaders;
using Client.Monogame.Plugin;
using Client.Monogame.Scenes;
using Microsoft.Xna.Framework;
using Pokemeow.Engine;
using Pokemeow.Engine.Input;
using Pokemeow.Engine.Plugin.Api;
using Pokemeow.Engine.Settings;
using Pokemeow.Engine.World;

namespace Client.Monogame
{
#if WINDOWS || LINUX
    /// <summary>
    ///     The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            using (var game = new Pokemeow())
            {
                game.Services.AddService<ISettingsManager>(new DummySettingsManager());
                // The GraphicsDeviceManager adds itself to the services with the interface IGraphicsDeviceManager, but we want it to be reachable with GraphicsDeviceManager, too
                game.Services.AddService(new GraphicsDeviceManager(game));
                game.Services.AddService<ISceneManager>(BuildSceneManager(game.Services));
                game.Services.AddService(new ModManager());
                game.Services.AddService<IGameComponentList>(new GameComponentList());
                game.Services.AddService<IWorldService>(new DummyWorldService(game.Services));
                var camera2D = new Camera2D();
                game.Services.AddService<ICamera>(camera2D);
                game.Services.AddService(camera2D);
                game.Services.AddService<IWorldDrawTarget>(new DummyWorldDrawTarget(game.Services));
                game.Services.AddService<IDrawableConverter>(new DummyDrawableConverter(game.Services));
                game.Services.AddService<Microsoft.Xna.Framework.Content.ContentManager>(game.Content);
                game.Services.AddService(new DisposableContainer());
                game.Services.AddService<IContentLoader<Map>>(new MapLoader(game.Services));
                game.Services.AddService<IController>(new MultiController(new KeyboardController(), new GamepadController()));
                game.Run();
            }
        }

        private static SceneManager BuildSceneManager(IServiceProvider serviceProvider)
        {
            var sceneManager = new SceneManager();
            sceneManager.AddScene(new WorldScene(serviceProvider));
            return sceneManager;
        }
    }
#endif
}