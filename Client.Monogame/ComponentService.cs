﻿using Microsoft.Xna.Framework;

namespace Client.Monogame
{
    /// <summary>
    ///     A service that returns all known components.
    /// </summary>
    public class ComponentService
    {
        public ComponentService(GameComponentCollection components)
        {
            Components = components;
        }

        /// <summary>
        ///     The known components. Is a direct reference to the original collection.
        /// </summary>
        public GameComponentCollection Components { get; }
    }
}