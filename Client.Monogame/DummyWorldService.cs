﻿using System;
using Pokemeow.Engine.World;
using ContentMgr = Microsoft.Xna.Framework.Content.ContentManager;

namespace Client.Monogame
{
    public class DummyWorldService : IWorldService
    {
        private readonly IServiceProvider _services;
        private Map _map;

        public DummyWorldService(IServiceProvider services)
        {
            _services = services;
        }

        public Map GetActiveMap()
        {
            if (_map != null) return _map;
            var content = (ContentMgr) _services.GetService(typeof (ContentMgr));
            _map = content.Load<Map>("map.default");
            return _map;
        }

        public event EventHandler ActiveMapChanged;
    }
}