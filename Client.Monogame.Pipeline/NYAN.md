﻿# The NYAN-format (V1.0)

## Introduction

The nyan-format is the current format for maps. It only contains information
about the objects placed on the map, HOW it looks is determined by another
file. The contents are stored in JSON.

## Format

### General Format

> {
>	'header': {
>		// See Header
>	},
>	'objects': [
>		// See Objects
>	],
>	'scripts': {
		// See Scripts
>	}
> }

### Header

The header contains basic information that are needed to compute the map.

Key | Possible Values | Description
---|---|---
id|string|The id of the map. Should be the same as the filename, must be
unique.
name|string|The name of the map. Can be displayed to the user.
description|string|A description of the map. Can be displayed to the user.
(Optional)
width|a positive integer|The width of the map, counted in tiles
height|a positive integer|The height of the map, counted in tiles

### Objects

The objects-section contains all objects that are rendered as parts of the map.
Note that these objects do not contain any information about how the objects 
look, to make different rendering-methods possible without recreating the maps.

Objects can contain other objects, so the map can be seen as a scenegraph. For
example, a house may have a door as child so door-animations don't affect the 
whole house.

The coordinates of children-objects are relative to the coordinates of the
parent-object.

An object is everything on a map: The floor, a house, NPCs, etc. What
properties they have differs by the type, but all objects share the following
properties:

Key | Possible Values | Description
---|---|---
id|string|The id of the object. Must be unique (on this map)
type|string|The type of the object (NPC, house, etc. Possible values below.)
coordinates|Coordinates|The coordinates of the top-left corner of the object.
For a description of the Coordinates-object, see below.
width|a positive integer|The width of the object, counted in tiles
height|a positive integer|The height of the object, counted in tiles
children|a list of objects|The children of this object.

#### Object-types

#### Floor

*Type-string:* floor

No further properties

#### House

*Type-string:* house

No further properties

#### Tree

*Type-string:* tree

No further properties

#### NPC

*Type-string:* npc

Possible properties:

Key | Possible Values | Description
---|---|---
movement|Reference to Movement-Data id|How the NPC should move while idling.
script|Reference to Script-Data id|The script that should be executed when the
player talks to the NPC

#### Talkable Field

*Type-string:* talkable

*Descritpion:* A Talkable field is a field that activates a script when the
player talks to it. An example is a shield.

Possible properties:

Key | Possible Values | Description
---|---|---
script|Reference to the Script-Data id|The script that should be executed when
the player activates the field.

#### Walkable Scriptfield

*Type-string:* script

*Description:* A field that activates a script when the player walks on it. 

Possible properties:

Key | Possible Values | Description
---|---|---
script|Reference to the Script-Data id|The script that should be executed when
the player walks on the field.

#### Substructures

##### Coordinates

Key | Possible Values | Description
---|---|---
x|a positive integer|The x component
y|a positive integer|The y component
z|a positive integer|The z component. Optional.

### Scripts

The Scripts section of the format describes map-wide scripts. For a
documentation about scripting in general, see TODO

The scripts-section contains the following properties:

Key | Possible Values | Description
---|---|---
mapscripts|an array of Script-Data ids|An array of scripts that should be
executed when the map is loaded. (Optional)

## Example Map

TODO