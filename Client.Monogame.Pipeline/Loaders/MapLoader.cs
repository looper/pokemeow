﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Newtonsoft.Json.Linq;
using Pokemeow.Engine.World;
using Pokemeow.Engine.World.Objects;
using Pokemeow.Engine.World.Objects.Environment;
using Pokemeow.Engine.World.Objects.Person;

namespace Client.Monogame.Pipeline.Loaders
{
    /// <summary>
    ///     Loads NYAN-maps.
    /// </summary>
    public class MapLoader : IContentLoader<Map>
    {
        // TODO code is shit
        private readonly IServiceProvider _provider;

        public MapLoader(IServiceProvider provider)
        {
            _provider = provider;
        }

        public Map Load(string assetname, string rootDir)
        {
            try
            {
                var path = Path.Combine(rootDir, "maps", assetname + ".nyan");
                using (var reader = new StreamReader(new FileStream(path, FileMode.Open)))
                {
                    var mapData = JObject.Parse(reader.ReadToEnd());
                    var id = mapData["header"]["id"].Value<string>();
                    var name = mapData["header"]["name"].Value<string>();
                    var description = mapData["header"]["description"].Value<string>();
                    var width = mapData["header"]["width"].Value<int>();
                    var height = mapData["header"]["height"].Value<int>();

                    var map = new Map(id);
                    var children = GetChildren(mapData["objects"].Children());

                    foreach (var child in children)
                    {
                        map.Add(child);
                    }

                    map.Add(new PlayerCharacter("player", new Rectangle(1, 1, 1, 1), _provider));

                    return map;
                }
            }
            catch (Exception e)
            {
                throw new ContentLoadException("Couldn't load map.", e);
            }
        }

        private IEnumerable<WorldObject> GetChildren(IEnumerable<JToken> children)
        {
            foreach (var child in children)
            {
                var childId = child["id"].Value<string>();
                var childType = child["type"].Value<string>();
                var coordinatesX = child["coordinates"]["x"].Value<int>();
                var coordinatesY = child["coordinates"]["y"].Value<int>();
                var childWidth = child["width"].Value<int>();
                var childHeight = child["height"].Value<int>();
                var children2 = child["children"];
                IEnumerable<WorldObject> moreChildren = new WorldObject[0];
                if (children2 != null && children2.HasValues)
                    moreChildren = GetChildren(children2.Children());

                var bounds = new Rectangle(coordinatesX, coordinatesY, childWidth, childHeight);

                WorldObject result;
                switch (childType)
                {
                    case "floor":
                        result = new Floor(childId, bounds, _provider);
                        break;
                    case "house":
                        result = new Building(childId, bounds, _provider);
                        break;
                    case "talkable":
                        continue;
                    default:
                        continue;
                }

                foreach (var worldObject in moreChildren)
                {
                    result.Add(worldObject);
                }
                yield return result;
            }
        }
    }
}