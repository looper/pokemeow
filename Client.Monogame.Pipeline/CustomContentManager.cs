﻿using System;
using Microsoft.Xna.Framework.Content;

namespace Client.Monogame.Pipeline
{
    /// <summary>
    ///     Custon Content Manager because the monogame implementation sucks. Why do I have to use the binary pipeline? I don't
    ///     want to.
    /// </summary>
    public class CustomContentManager : ContentManager
    {
        private readonly IServiceProvider _serviceProvider;

        public CustomContentManager(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public CustomContentManager(IServiceProvider serviceProvider, string rootDirectory)
            : base(serviceProvider, rootDirectory)
        {
            _serviceProvider = serviceProvider;
        }

        public override T Load<T>(string assetName)
        {
            var loadService = (IContentLoader<T>) _serviceProvider.GetService(typeof (IContentLoader<T>));
            if (loadService != null)
                return loadService.Load(assetName, RootDirectory);
            return base.Load<T>(assetName);
        }
    }
}