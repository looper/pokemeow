﻿namespace Client.Monogame.Pipeline
{
    /// <summary>
    ///     Interface for custom content loaders.
    /// </summary>
    /// <typeparam name="T">The type of the content that should be loaded.</typeparam>
    public interface IContentLoader<out T>
    {
        /// <summary>
        ///     Tries to load an asset with the given name.
        /// </summary>
        /// <param name="assetname"></param>
        /// <returns></returns>
        T Load(string assetname, string rootDir);
    }
}