﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Pokemeow.Engine
{
    /// <summary>
    ///     A service that can hold many Disposables and disposes them when needed.
    /// </summary>
    public class DisposableContainer : IDisposable
    {
        private readonly ISet<IDisposable> _disposables = new HashSet<IDisposable>();

        public void Add(IDisposable disposable)
        {
            _disposables.Add(disposable);
        }

        public void Dispose()
        {
            foreach (var disposable in _disposables)
            {
                disposable.Dispose();
            }
        }
    }
}