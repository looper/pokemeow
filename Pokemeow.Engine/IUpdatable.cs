﻿using System;
using Microsoft.Xna.Framework;

namespace Pokemeow.Engine
{
    /// <summary>
    ///     Interface for all updatable entites.
    /// </summary>
    public interface IUpdatable
    {
        /// <summary>
        ///     Updates the state of the scene.
        /// </summary>
        /// <param name="elapsedTime">The time that elapsed since the last update.</param>
        void Update(GameTime elapsedTime);
    }
}