﻿namespace Pokemeow.Engine
{
    /// <summary>
    ///     Provides as identifier. This identifier should be unique between all instances of the same class.
    /// </summary>
    public interface IIdentifiable
    {
        string Identifier { get; }
    }
}