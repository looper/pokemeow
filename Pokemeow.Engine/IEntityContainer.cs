﻿using System.Collections.Generic;

namespace Pokemeow.Engine
{
    /// <summary>
    ///     Can hold multiple entities as children.
    /// </summary>
    public interface IEntityContainer<T> where T : IEntity
    {
        IEnumerable<T> Children { get; }
        void Add(T child);
        void Remove(T child);
    }
}