﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Client.Common.Scenes
{
    public class SceneManager : ISceneManager
    {
        private readonly Dictionary<Type, IScene> _scenes = new Dictionary<Type, IScene>();
        private Type _currentSceneType;

        public void Draw(TimeSpan elapsedTime)
        {
            if (_currentSceneType == null)
                throw new Exception("No scene available!");
            _scenes[_currentSceneType].Draw(elapsedTime);
        }

        public void Update(GameTime elapsedTime)
        {
            if (_currentSceneType == null)
                throw new Exception("No scene available!");
            _scenes[_currentSceneType].Update(elapsedTime);
        }

        public void AddScene<T>(T scene) where T : IScene
        {
            _scenes.Add(typeof (T), scene);
        }

        public IScene GetCurrentScene()
        {
            if (_currentSceneType == null)
                return null;
            return _scenes[_currentSceneType];
        }

        public void EnterScene<T>() where T : IScene
        {
            if (!_scenes.ContainsKey(typeof (T)))
                throw new Exception("No applicable scene found.");
            if (_currentSceneType != null)
                _scenes[_currentSceneType].Leave();
            _currentSceneType = typeof (T);
            if (!_scenes[_currentSceneType].IsInitialized)
                _scenes[_currentSceneType].Initialize();
            _scenes[_currentSceneType].Enter();
        }
    }
}