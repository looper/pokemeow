﻿using Pokemeow.Engine;
using Pokemeow.Engine.Graphics;

namespace Client.Common.Scenes
{
    /// <summary>
    ///     Manages the scenes.
    /// </summary>
    public interface ISceneManager : IDrawable, IUpdatable
    {
        /// <summary>
        ///     Adds the scene to all known scenes. To make the scene active, call EnterScene afterwards.
        ///     The scene will be identified by its type, only one scene per type can exist. If you try to add a second scene to
        ///     the same type, the old scene will be overwritten.
        /// </summary>
        /// <typeparam name="T">The type of the scene.</typeparam>
        /// <param name="scene"></param>
        void AddScene<T>(T scene) where T : IScene;

        /// <summary>
        ///     Returns the currently active scene.
        /// </summary>
        /// <returns>The currently active scene.</returns>
        IScene GetCurrentScene();

        /// <summary>
        ///     Enters the scene of the given type.
        /// </summary>
        /// <typeparam name="T">The type of the scene</typeparam>
        void EnterScene<T>() where T : IScene;
    }
}