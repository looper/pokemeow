﻿using Pokemeow.Engine;
using Pokemeow.Engine.Graphics;

namespace Client.Common.Scenes
{
    /// <summary>
    ///     A state that can be organized using the scenemanager.
    /// </summary>
    public interface IScene : IDrawable, IUpdatable
    {
        /// <summary>
        ///     The name of the scene. Can be used to identify the screen.
        /// </summary>
        string Name { get; }

        /// <summary>
        ///     Whether the scene is already initialized.
        /// </summary>
        bool IsInitialized { get; }

        /// <summary>
        ///     Loads the resources the scene may need.
        /// </summary>
        void Initialize();

        /// <summary>
        ///     Unloads the resources the scene needed.
        /// </summary>
        void DeInitialize();

        /// <summary>
        ///     Enters the scene. From this point on, update and draw may be called.
        /// </summary>
        void Enter();

        /// <summary>
        ///     Leaves the scene. Must not unload resources loaded in #Load, because the state might be reentered. From this point
        ///     on, update and draw won't be called until the next time to #Enter.
        /// </summary>
        void Leave();
    }
}