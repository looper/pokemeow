﻿using System;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Xna.Framework;

namespace Pokemoew.Engine.Scripting.Api
{
    /// <summary>
    ///     A script that can influence the world.
    /// </summary>
    public interface IScript
    {
        /// <summary>
        ///     Executes the script.
        /// </summary>
        /// <param name="caller">The object that calls the script.</param>
        /// <typeparam name="TCaller">
        ///     The type of the caller. The caller can be any object that is responsible for executing a
        ///     script, most likely a script-field or a npc.
        /// </typeparam>
        void Execute<TCaller>(TCaller caller);

        /// <summary>
        ///     Executes the script. This method is for scripts that are executed in every Frame, like movement-scripts.
        /// </summary>
        /// <param name="caller">The object that calls the script.</param>
        /// <typeparam name="TCaller">
        ///     The type of the caller. The caller can be any object that is responsible for executing a
        ///     script, most likely a script-field or a npc.
        /// </typeparam>
        /// <param name="elapsedTime"></param>
        void ExecuteFromFrame<TCaller>(TCaller caller, GameTime elapsedTime);
    }
}