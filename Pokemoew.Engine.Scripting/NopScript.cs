﻿using System;
using Microsoft.Xna.Framework;

namespace Pokemoew.Engine.Scripting.Api
{
    /// <summary>
    ///     A script that does nothing.
    /// </summary>
    public class NopScript : IScript
    {
        public void Execute<TCaller>(TCaller caller)
        {
        }

        public void ExecuteFromFrame<TCaller>(TCaller caller, GameTime elapsedTime)
        {
        }
    }
}