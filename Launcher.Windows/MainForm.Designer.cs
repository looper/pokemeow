﻿namespace Launcher.Windows
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.launchButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageNews = new System.Windows.Forms.TabPage();
            this.tabPageMods = new System.Windows.Forms.TabPage();
            this.newsBrowser = new System.Windows.Forms.WebBrowser();
            this.modButtonsPanel = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.modList = new System.Windows.Forms.ListBox();
            this.addModButton = new System.Windows.Forms.Button();
            this.removeModButton = new System.Windows.Forms.Button();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageNews.SuspendLayout();
            this.tabPageMods.SuspendLayout();
            this.modButtonsPanel.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.launchButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 236);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(284, 26);
            this.panel1.TabIndex = 0;
            // 
            // launchButton
            // 
            this.launchButton.Location = new System.Drawing.Point(206, 0);
            this.launchButton.Name = "launchButton";
            this.launchButton.Size = new System.Drawing.Size(75, 23);
            this.launchButton.TabIndex = 0;
            this.launchButton.Text = "Launch";
            this.launchButton.UseVisualStyleBackColor = true;
            this.launchButton.Click += new System.EventHandler(this.launchButton_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageNews);
            this.tabControl1.Controls.Add(this.tabPageMods);
            this.tabControl1.Controls.Add(this.tabPageSettings);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(284, 236);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPageNews
            // 
            this.tabPageNews.Controls.Add(this.newsBrowser);
            this.tabPageNews.Location = new System.Drawing.Point(4, 22);
            this.tabPageNews.Name = "tabPageNews";
            this.tabPageNews.Size = new System.Drawing.Size(276, 210);
            this.tabPageNews.TabIndex = 0;
            this.tabPageNews.Text = "News";
            this.tabPageNews.UseVisualStyleBackColor = true;
            // 
            // tabPageMods
            // 
            this.tabPageMods.Controls.Add(this.panel3);
            this.tabPageMods.Controls.Add(this.modButtonsPanel);
            this.tabPageMods.Location = new System.Drawing.Point(4, 22);
            this.tabPageMods.Name = "tabPageMods";
            this.tabPageMods.Size = new System.Drawing.Size(276, 210);
            this.tabPageMods.TabIndex = 1;
            this.tabPageMods.Text = "Mods";
            this.tabPageMods.UseVisualStyleBackColor = true;
            // 
            // newsBrowser
            // 
            this.newsBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newsBrowser.Location = new System.Drawing.Point(0, 0);
            this.newsBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.newsBrowser.Name = "newsBrowser";
            this.newsBrowser.Size = new System.Drawing.Size(276, 210);
            this.newsBrowser.TabIndex = 0;
            this.newsBrowser.Url = new System.Uri("about:blank", System.UriKind.Absolute);
            // 
            // modButtonsPanel
            // 
            this.modButtonsPanel.Controls.Add(this.removeModButton);
            this.modButtonsPanel.Controls.Add(this.addModButton);
            this.modButtonsPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.modButtonsPanel.Enabled = false;
            this.modButtonsPanel.Location = new System.Drawing.Point(202, 0);
            this.modButtonsPanel.Name = "modButtonsPanel";
            this.modButtonsPanel.Size = new System.Drawing.Size(74, 210);
            this.modButtonsPanel.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.modList);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(202, 210);
            this.panel3.TabIndex = 2;
            // 
            // modList
            // 
            this.modList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.modList.FormattingEnabled = true;
            this.modList.Location = new System.Drawing.Point(0, 0);
            this.modList.Name = "modList";
            this.modList.Size = new System.Drawing.Size(202, 210);
            this.modList.TabIndex = 1;
            // 
            // addModButton
            // 
            this.addModButton.Location = new System.Drawing.Point(6, 3);
            this.addModButton.Name = "addModButton";
            this.addModButton.Size = new System.Drawing.Size(60, 23);
            this.addModButton.TabIndex = 0;
            this.addModButton.Text = "Add";
            this.addModButton.UseVisualStyleBackColor = true;
            // 
            // removeModButton
            // 
            this.removeModButton.Location = new System.Drawing.Point(6, 32);
            this.removeModButton.Name = "removeModButton";
            this.removeModButton.Size = new System.Drawing.Size(60, 23);
            this.removeModButton.TabIndex = 1;
            this.removeModButton.Text = "Remove";
            this.removeModButton.UseVisualStyleBackColor = true;
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSettings.Size = new System.Drawing.Size(276, 210);
            this.tabPageSettings.TabIndex = 2;
            this.tabPageSettings.Text = "Settings";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Launcher";
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageNews.ResumeLayout(false);
            this.tabPageMods.ResumeLayout(false);
            this.modButtonsPanel.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button launchButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageNews;
        private System.Windows.Forms.WebBrowser newsBrowser;
        private System.Windows.Forms.TabPage tabPageMods;
        private System.Windows.Forms.Panel modButtonsPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListBox modList;
        private System.Windows.Forms.Button removeModButton;
        private System.Windows.Forms.Button addModButton;
        private System.Windows.Forms.TabPage tabPageSettings;
    }
}

