﻿using Pokemoew.Engine.Scripting.Api;

namespace Pokemeow.Engine.World
{
    /// <summary>
    ///     Interface for every WorldObject that can execute a script in any way.
    /// </summary>
    public interface IScriptable
    {
        /// <summary>
        ///     The script that is executed when this WorldObject is asked to execute.
        /// </summary>
        IScript Script { get; }
    }
}