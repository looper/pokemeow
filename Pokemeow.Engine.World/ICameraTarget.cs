﻿using Microsoft.Xna.Framework;

namespace Pokemeow.Engine.World
{
    /// <summary>
    ///     A target which is followed by the camera.
    /// </summary>
    public interface ICameraTarget
    {
        /// <summary>
        ///     The position (as pixels relative from the top-left map-corner) of the target.
        /// </summary>
        Vector2 Position { get; }
    }
}