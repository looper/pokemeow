﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Pokemeow.Engine.Input;
using Pokemoew.Engine.Scripting.Api;

namespace Pokemeow.Engine.World.Objects.Person
{
    /// <summary>
    ///     The main character that is controlled by the player.
    /// </summary>
    public class PlayerCharacter : Npc
    {
        public PlayerCharacter(string identifier, Rectangle bounds, IServiceProvider services)
            : base(identifier, bounds, new NopScript(), new PlayerMovementScript(services), services)
        {
        }

        private class PlayerMovementScript : IScript, IControllable, ICameraTarget
        {
            private const float MovementSpeed = 1.0F;

            private readonly IServiceProvider _services;
            private IEnumerable<ControllerAction> _lastActions = new ControllerAction[0];

            public PlayerMovementScript(IServiceProvider services)
            {
                _services = services;
                ((IController) _services.GetService(typeof (IController))).RegisterControllable(this);
                ((ICamera) _services.GetService(typeof (ICamera))).Follow(this);
            }

            public Vector2 Position { get; private set; }

            public void PerformActions(IEnumerable<ControllerAction> actions)
            {
                _lastActions = actions;
            }

            public void PerformClick(Vector2 coordinates, int action = 1)
            {
                // TODO movement per mouse later?
            }

            public void Execute<TCaller>(TCaller caller)
            {
                throw new NotImplementedException("The PlayerCharacter cant be moved this way!");
            }

            public void ExecuteFromFrame<TCaller>(TCaller caller, GameTime elapsedTime)
            {
                var world = (IWorldService)_services.GetService(typeof(IWorldService));
                var player = caller as PlayerCharacter;
                if (player == null)
                    throw new ArgumentException("Unknown caller!");
                Position = ((ICameraTarget) player).Position;
                foreach (var action in _lastActions.Distinct())
                {
                    switch (action)
                    {
                        case ControllerAction.Down:
                            player.Bounds = GetUpdatedBounds(player, 0, 1);
                            break;
                        case ControllerAction.Up:
                            player.Bounds = GetUpdatedBounds(player, 0, -1);
                            break;
                        case ControllerAction.Left:
                            player.Bounds = GetUpdatedBounds(player, -1, 0);
                            break;
                        case ControllerAction.Right:
                            player.Bounds = GetUpdatedBounds(player, 1, 0);
                            break;
                    }
                }
                // TODO FIXME fix that speed, it's framedependend :(((((
            }

            public void PerformAction(ControllerAction action)
            {
                _lastActions = new[] {action};
            }

            private Rectangle GetUpdatedBounds(PlayerCharacter player, int x, int y)
            {
                var world = (IWorldService)_services.GetService(typeof(IWorldService));
                if (world.GetActiveMap().IsWalkable(new Vector2(player.Bounds.X + x, player.Bounds.Y + y), player))
                    return new Rectangle(new Point(player.Bounds.X + x, player.Bounds.Y + y), player.Bounds.Size);
                return player.Bounds;
            }
        }
    }
}