﻿using System;
using Microsoft.Xna.Framework;
using Pokemeow.Engine.Graphics;
using Pokemoew.Engine.Scripting.Api;

namespace Pokemeow.Engine.World.Objects.Person
{
    /// <summary>
    ///     A character that can be placed in the world and interacted with.
    /// </summary>
    public class Npc : WorldObject, IScriptable
    {
        private readonly IScript _movementScript;
        private readonly IServiceProvider _services;

        public Npc(string identifier, Rectangle bounds, IScript activationScript,
            IScript movementScript, IServiceProvider services) : base(identifier, bounds)
        {
            _movementScript = movementScript;
            _services = services;
            Script = activationScript;
        }

        public IScript Script { get; }

        public override void Update(GameTime elapsedTime)
        {
            _movementScript.ExecuteFromFrame(this, elapsedTime);
            base.Update(elapsedTime);
        }

        public override void Draw(TimeSpan elapsedTime)
        {
            // Todo fixme movement animation?
            var target = _services.GetService(typeof(IWorldDrawTarget)) as IDrawTarget;
            if (target == null)
                throw new NullReferenceException("Couldn't get a WorldDrawTarget.");
            var converter = _services.GetService(typeof(IDrawableConverter)) as IDrawableConverter;
            if (converter == null)
                throw new NullReferenceException("Couldn't get a DrawableConverter.");
            target.Draw(elapsedTime, converter.Convert(this, Identifier), Bounds);
            base.Draw(elapsedTime);
        }

        public override bool IsWalkable(Vector2 position, WorldObject sourceObject)
        {
            return false;
        }
    }
}