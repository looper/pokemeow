﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Pokemeow.Engine.Graphics;

namespace Pokemeow.Engine.World.Objects
{
    /// <summary>
    ///     An object that is placed on a map. Could be a house, a person, a scriptfield, etc.
    /// </summary>
    public abstract class WorldObject : IUiEntity, IEntityContainer<WorldObject>, IIdentifiable, ICameraTarget
    {
        private readonly List<WorldObject> _children = new List<WorldObject>();

        protected WorldObject(string identifier, Rectangle bounds)
        {
            Bounds = bounds;
            Identifier = identifier;
        }

        public Vector2 Position => Bounds.Location.ToVector2()*16;

        public IEnumerable<WorldObject> Children => _children.AsReadOnly();

        public void Add(WorldObject child)
        {
            _children.Add(child);
        }

        public void Remove(WorldObject child)
        {
            _children.Remove(child);
        }

        public string Identifier { get; }

        public Rectangle Bounds { get; set; }

        public virtual void Update(GameTime elapsedTime)
        {
            foreach (var child in Children)
            {
                child.Update(elapsedTime);
            }
        }

        public virtual void Draw(TimeSpan elapsedTime)
        {
            foreach (var child in Children)
            {
                child.Draw(elapsedTime);
            }
        }

        /// <summary>
        /// Asks the object whether the sourceObject could walk on the object.
        /// </summary>
        /// <param name="position">The position measured in tiles, relative to the top-left corner of the object</param>
        /// <param name="sourceObject"></param>
        public virtual bool IsWalkable(Vector2 position, WorldObject sourceObject)
        {
            var child = Children.FirstOrDefault(c => c.Bounds.Contains(position));
            return child?.IsWalkable(position, sourceObject) ?? DefaultWalkable();
        }

        /// <summary>
        /// Returns the default walkable property of this object.
        /// </summary>
        /// <returns></returns>
        protected virtual bool DefaultWalkable()
        {
            return true;
        }
    }
}