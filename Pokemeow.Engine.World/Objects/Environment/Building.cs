﻿using System;
using Microsoft.Xna.Framework;
using Pokemeow.Engine.Graphics;

namespace Pokemeow.Engine.World.Objects.Environment
{
    /// <summary>
    ///     A building that can be placed on an outside map.
    /// </summary>
    public class Building : WorldObject
    {
        private readonly IServiceProvider _services;

        public Building(string id, Rectangle bounds, IServiceProvider services)
            : base(id, bounds)
        {
            _services = services;
        }

        public override void Draw(TimeSpan elapsedTime)
        {
            var target = _services.GetService(typeof (IWorldDrawTarget)) as IDrawTarget;
            if (target == null)
                throw new NullReferenceException("Couldn't get a WorldDrawTarget.");
            var converter = _services.GetService(typeof (IDrawableConverter)) as IDrawableConverter;
            if (converter == null)
                throw new NullReferenceException("Couldn't get a DrawableConverter.");
            target.Draw(elapsedTime, converter.Convert(this, Identifier), Bounds);
            base.Draw(elapsedTime);
        }

        // TODO do some building-specific stuff

        protected override bool DefaultWalkable()
        {
            return false;
        }
    }
}