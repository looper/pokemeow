﻿using Microsoft.Xna.Framework;

namespace Pokemeow.Engine.World
{
    /// <summary>
    ///     Converts world-coordinates to screen-coordinates and vice-versa.
    /// </summary>
    public static class WorldCoordinatesConversion
    {
        public static Vector2 ConvertToWorldCoordinates(this Vector2 source)
        {
            // todo world offset? oO
            return source/16;
        }

        public static Vector2 ConvertToScreenCoordinates(this Vector2 source)
        {
            // todo world offset? oO
            return source*16;
        }
    }
}