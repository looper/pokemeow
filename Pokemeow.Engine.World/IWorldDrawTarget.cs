﻿using Pokemeow.Engine.Graphics;

namespace Pokemeow.Engine.World
{
    /// <summary>
    ///     A DrawTarget that draws the objects onto the world. The position is the position on the complete map.
    /// </summary>
    public interface IWorldDrawTarget : IDrawTarget
    {
    }
}