﻿using Pokemeow.Engine.Graphics;

namespace Pokemeow.Engine.World
{
    /// <summary>
    ///     Converts a drawable that can't provide it's own texture to an actual drawable.
    /// </summary>
    public interface IDrawableConverter
    {
        IDrawable Convert(IDrawable drawable, string id);
    }
}