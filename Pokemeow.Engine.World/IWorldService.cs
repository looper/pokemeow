﻿using System;

namespace Pokemeow.Engine.World
{
    /// <summary>
    ///     Holds information about the currently active things inside the world (such as the current map).
    /// </summary>
    public interface IWorldService
    {
        /// <summary>
        ///     Returns the currently active map.
        /// </summary>
        /// <returns></returns>
        Map GetActiveMap();

        /// <summary>
        ///     Is triggered when the map changes.
        /// </summary>
        event EventHandler ActiveMapChanged;
    }
}