﻿namespace Pokemeow.Engine.World
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICamera : IUpdatable
    {
        void Follow(ICameraTarget target);
    }
}