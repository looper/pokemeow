﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Pokemeow.Engine.Graphics;
using Pokemeow.Engine.World.Objects;

namespace Pokemeow.Engine.World
{
    /// <summary>
    ///     The map that can be displayed in the world. Contains all GameObjects to be rendered.
    /// </summary>
    public class Map : IUiEntity, IEntityContainer<WorldObject>, IIdentifiable
    {
        private readonly List<WorldObject> _children = new List<WorldObject>();

        public Map(string identifier)
        {
            Identifier = identifier;
        }

        /// <summary>
        ///     The objects that are available in the world. The returned enumerable is read-only.
        /// </summary>
        public IEnumerable<WorldObject> Children => _children.AsReadOnly();

        /// <summary>
        ///     Adds a object to the map. The order in which the objects are added is important; they will be rendered in that
        ///     order.
        /// </summary>
        /// <param name="child"></param>
        public void Add(WorldObject child)
        {
            _children.Add(child);
        }

        public void Remove(WorldObject child)
        {
            _children.Remove(child);
        }

        public string Identifier { get; }

        public void Update(GameTime elapsedTime)
        {
            foreach (var child in Children)
            {
                child.Update(elapsedTime);
            }
        }

        public void Draw(TimeSpan elapsedTime)
        {
            foreach (var child in Children)
            {
                child.Draw(elapsedTime);
            }
        }

        public int Layer
        {
            get { return 0; }
            set { }
        }

        public bool IsWalkable(Vector2 position, WorldObject sourceEntity)
        {
            var worldObject = Children.FirstOrDefault(obj => obj.Bounds.Contains(position));
            return worldObject == null || worldObject.IsWalkable(position - worldObject.Position, sourceEntity);
        }
    }
}