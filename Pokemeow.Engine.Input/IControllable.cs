﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Pokemeow.Engine.Input
{
    /// <summary>
    ///     Interface for all entities that can receive controller-events.
    /// </summary>
    public interface IControllable
    {
        /// <summary>
        ///     Performs multiple actions. Either this method or PerformAction will be called in one frame, not both.
        /// </summary>
        /// <param name="actions"></param>
        void PerformActions(IEnumerable<ControllerAction> actions);

        /// <summary>
        ///     Performs a click on the given coordinates. The action-parameter can be used (e.g. for mouse-clicks) to get the
        ///     button that was clicked. Up to 32 buttons are supported.
        /// </summary>
        /// <param name="coordinates">The position where the click was performed. Is absolute from the upper-left screen corner.</param>
        /// <param name="action">
        ///     Contains info about what button was pressed. The buttons that were pressed are determined by the
        ///     bits, e.g. the bit 0 tells that the mouse-button 1 was pressed, etc.
        /// </param>
        void PerformClick(Vector2 coordinates, int action = 1);
    }
}