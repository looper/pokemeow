﻿using Microsoft.Xna.Framework;

namespace Pokemeow.Engine.Input
{
    /// <summary>
    ///     Interface for the controller. A controller might be keyboard and mouse, an actual controller or something else..
    /// </summary>
    public interface IController
    {
        /// <summary>
        ///     Registers a controllable that will be notified when an action is performed.
        /// </summary>
        /// <param name="controllable"></param>
        void RegisterControllable(IControllable controllable);

        /// <summary>
        /// Updates the controller.
        /// </summary>
        /// <param name="elapsedTime"></param>
        void Update(GameTime elapsedTime);
    }
}