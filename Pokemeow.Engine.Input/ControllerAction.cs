﻿namespace Pokemeow.Engine.Input
{
    /// <summary>
    ///     Actions that can be performed by a controller, e.g. movement or a menu-button.
    /// </summary>
    public enum ControllerAction
    {
        Up,
        Left,
        Right,
        Down,
        Menu
    }
}