﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Pokemeow.Engine.Input;

namespace Client.Monogame.Controller
{
    /// <summary>
    ///     A controller that utilizes a gamepad.
    /// </summary>
    public class GamepadController : IController
    {
        // todo maybe make controller movements speed-sensitive?

        private const float Deadzone = 0.5F;
        private readonly List<IControllable> _controllables = new List<IControllable>();
        private readonly Dictionary<Buttons, ButtonState> _lastStates = new Dictionary<Buttons, ButtonState>();

        public void RegisterControllable(IControllable controllable)
        {
            _controllables.Add(controllable);
        }

        public void Update(GameTime elapsedTime)
        {
            var state = GamePad.GetState(PlayerIndex.One);
            if (!state.IsConnected)
                return;

            var actions = new List<ControllerAction>();

            if (_lastStates.ContainsKey(Buttons.Start) && state.Buttons.Start != _lastStates[Buttons.Start])
            {
                actions.Add(ControllerAction.Menu);
            }
            if (state.ThumbSticks.Left.X > Deadzone)
                actions.Add(ControllerAction.Right);
            if (state.ThumbSticks.Left.X < -1*Deadzone)
                actions.Add(ControllerAction.Left);
            if (state.ThumbSticks.Left.Y > Deadzone)
                actions.Add(ControllerAction.Down);
            if (state.ThumbSticks.Left.Y < -1*Deadzone)
                actions.Add(ControllerAction.Up);

            foreach (var controllable in _controllables)
            {
                controllable.PerformActions(actions);
            }

            _lastStates[Buttons.Start] = state.Buttons.Start;
        }
    }
}