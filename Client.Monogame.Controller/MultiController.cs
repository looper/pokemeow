﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Pokemeow.Engine.Input;

namespace Client.Monogame.Controller
{
    /// <summary>
    ///     A controller that can combine multiple controllers.
    /// </summary>
    public class MultiController : IController
    {
        private readonly ICollection<IController> _controller;

        public MultiController(ICollection<IController> controller)
        {
            _controller = controller;
        }

        public MultiController(params IController[] controller)
        {
            _controller = controller;
        }

        public void RegisterControllable(IControllable controllable)
        {
            foreach (var controller in _controller)
            {
                controller.RegisterControllable(controllable);
            }
        }

        public void Update(GameTime elapsedTime)
        {
            foreach (var controller in _controller)
            {
                controller.Update(elapsedTime);
            }
        }
    }
}