﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Pokemeow.Engine.Input;

namespace Client.Monogame.Controller
{
    /// <summary>
    ///     Controller that uses the Keyboard to send the events.
    /// </summary>
    public class KeyboardController : IController
    {
        private readonly IList<IControllable> _controllables = new List<IControllable>();

        public void RegisterControllable(IControllable controllable)
        {
            _controllables.Add(controllable);
        }

        public void Update(GameTime elapsedTime)
        {
            var state = Keyboard.GetState();

            var actions =
                state.GetPressedKeys().Select(ToControllerAction).Where(a => a.HasValue).Select(a => a.Value).ToList();
            foreach (var controllable in _controllables)
            {
                controllable.PerformActions(actions);
            }
        }

        private static ControllerAction? ToControllerAction(Keys key)
        {
            switch (key)
            {
                case Keys.A:
                case Keys.Left:
                    return ControllerAction.Left;
                case Keys.D:
                case Keys.Right:
                    return ControllerAction.Right;
                case Keys.W:
                case Keys.Up:
                    return ControllerAction.Up;
                case Keys.S:
                case Keys.Down:
                    return ControllerAction.Down;
                case Keys.Escape:
                case Keys.M:
                    return ControllerAction.Menu;
            }
            return null;
        }
    }
}