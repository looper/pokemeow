﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Client.Monogame.Plugin
{
    /// <summary>
    ///     Holds all GameComponents. Can be retrieved using the ServiceProvider.
    /// </summary>
    public interface IGameComponentList
    {
        /// <summary>
        ///     Adds a new GameComponent to the list. This musn't be called after the game is initialized.
        /// </summary>
        /// <param name="component"></param>
        void Add<T>(T component) where T : IGameComponent;

        /// <summary>
        ///     Gets all registered GameComponents.
        /// </summary>
        /// <returns></returns>
        IEnumerable<IGameComponent> GetAllComponents();

        /// <summary>
        ///     Gets the GameComponent with the given type. If no GameComponent can be found, null is returned.
        /// </summary>
        /// <typeparam name="T">The type of the wanted Component</typeparam>
        /// <returns>The component or null if no component was found</returns>
        T GetComponent<T>() where T : IGameComponent;
    }
}