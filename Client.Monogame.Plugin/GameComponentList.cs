﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Client.Monogame.Plugin
{
    public class GameComponentList : IGameComponentList
    {
        private readonly Dictionary<Type, IGameComponent> _components = new Dictionary<Type, IGameComponent>();

        public void Add<T>(T component) where T : IGameComponent
        {
            _components.Add(typeof (T), component);
        }

        public IEnumerable<IGameComponent> GetAllComponents()
        {
            return _components.Values;
        }

        public T GetComponent<T>() where T : IGameComponent
        {
            if (!_components.ContainsKey(typeof (T)))
                return default(T);
            return (T) _components[typeof (T)];
        }
    }
}