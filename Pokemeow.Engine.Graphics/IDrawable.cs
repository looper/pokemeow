﻿using System;

namespace Pokemeow.Engine.Graphics
{
    /// <summary>
    ///     Interface for all components that can be drawed.
    /// </summary>
    public interface IDrawable
    {
        /// <summary>
        ///     Redraws the scene.
        /// </summary>
        /// <param name="elapsedTime">The time that elapsed since the last draw.</param>
        void Draw(TimeSpan elapsedTime);
    }
}