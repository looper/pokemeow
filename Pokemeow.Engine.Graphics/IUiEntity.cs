﻿namespace Pokemeow.Engine.Graphics
{
    /// <summary>
    ///     An entity that can both be shown in the world and updated.
    /// </summary>
    public interface IUiEntity : IEntity, IDrawable
    {
    }
}