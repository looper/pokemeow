﻿using System;
using Microsoft.Xna.Framework;

namespace Pokemeow.Engine.Graphics
{
    /// <summary>
    ///     Can be used to draw drawables onto. Should be implemented as a service by the client engine.
    /// </summary>
    public interface IDrawTarget
    {
        /// <summary>
        ///     Draws the drawable onto this target at the specified position.
        /// </summary>
        /// <param name="elapsedTime"></param>
        /// <param name="drawable"></param>
        /// <param name="bounds"></param>
        void Draw(TimeSpan elapsedTime, IDrawable drawable, Rectangle bounds);
    }
}